# SINGLE ENGINE PISTON AEROPLANE ENDORSEMENT

### ENGINEERING, DATA AND PERFORMANCE QUESTIONNAIRE

##### Pathfinder Aviation

##### Tony Morris

##### ARN: 1007036

##### RA member: 045481

**1. General Aircraft Data**

What is the make, type and model of the aeroplane?

* Make:

> The Airplane Factory

* Type:

> Aeroplane

* Model:

> Sling 2 (SLG2 *ICAO designator*)

* In which categories is the aeroplane permitted to fly?

> Light Sport Aircraft (LSA)

----

**2. Airspeed Limitation**

List the applicable airspeeds for the aeroplane type and their meaning

* Vmax XW

> * 15kt
> * Maximum permitted cross wind speed for take-off and landing
> * *(PoH 2.4)*

* Va

> * 91KIAS
> * Maximum manouevring speed
> * *(PoH 2.2)*

* Vs0

> * 40KIAS
> * Minimum speed (above stall) to maintain level flight in landing configuration
> * *(PoH 2.2)*

* Vs1

> * 45KIAS
> * Minimum speed (above stall) to maintain level flight in clean configuration
> * *(PoH 2.2)*

* Vx

> * 65KIAS
> * Best angle of climb
> * The airspeed at which the minimum horizontal distance is achieved for any given altitude
> * *(PoH 4.5)*

* Vy

> * 72KIAS
> * Best rate of climb
> * The airspeed at which the minimum time is achieved for any given altitude
> * *(PoH 4.5)*

* Vfe

> * 85KIAS
> * Maximum speed to have flaps extended
> * *(PoH 2.2)*

* Vno

> * 120KIAS
> * Maximum structural cruise speed
> * "normal operations"
> * *(PoH 2.2)*

* Vne

> * 135KIAS
> * Maximum speed in any flight phase
> * "never exceed"
> * *(PoH 2.2)*

* Vglide

> * 70KIAS
> * Speed to achieve optimum ratio between vertical descent and lateral distance.
> * The airspeed at which induced and parasitic drag (total drag) intersect.
> * "best glide"
> * *(PoH 4.7)*

a) Maximum load factor (flaps up) is + ______ g and - ______ g; and

> * +4g
> * -2g
> * *(PoH 2.6)*

b) Maximum load factor (flaps down) is + ______ g and - ______ g; and

> * +2g
> * -1g
> * *(PoH 2.6)*

----

**3. Emergency Procedures**

Details the emergency procedures for the following situations

a) Engine fire on the ground (engine starting)

> * Release starter immediately
> * Close fuel selector
> * Close throttle
> * Ignition OFF
> * If possible, collect fire extinguisher from luggage compartment
> * Evacuate aircraft
> * If possible, extinguish fire
> * Contact fire department if necessary
> * *(PoH 3.3.1)*

b) Electrical fire on the ground

> * Close heating
> * Close fuel selector
> * Set throttle full open
> * Ignition OFF
> * If possible, collect fire extinguisher from luggage compartment
> * Evacuate aircraft
> * If possible, extinguish fire
> * Contact fire department if necessary
> * *(PoH 3.3.2)*

c) Engine fire airborne

> * Close heating
> * Close fuel selector
> * Set throttle full open
> * When engine stops due to fuel starvation, turn ignition OFF
> * Perform forced landing (with no engine restart attempt)
>   * 70KIAS
>   * Trim
>   * Select landing area
>   * Check/tighten safety harness
>   * Select flap as required
>   * Communicate location and intentions (tx 7700)
>   * Brief passenger
>   * Immediately before touch-down, fuel selector OFF
>   * Master switch OFF
>   * Fuel pump OFF
> * *(PoH 3.3.4)*
> * *(PoH 3.4.1)*

d) Electrical fire in flight

> * Close heating
> * All non-essential (e.g. ignition) electrics OFF
> * Land ASAP
> * Ignition OFF
> * Master switch OFF
> * If possible, collect fire extinguisher from luggage compartment
> * If possible, extinguish fire
> * Contact fire department if necessary

e) Cabin fire in flight

> * Close heating
> * Open fresh air intakes
> * All non-essential (e.g. ignition) electrics OFF
> * Land ASAP
> * Ignition OFF
> * Master switch OFF
> * If possible, collect fire extinguisher from luggage compartment
> * If possible, extinguish fire
> * Contact fire department if necessary

f) Engine failure after take-off

> * 70KIAS
> * Select landing area within 45 degrees either side of current heading
> * Flap as required
> * Throttle as required
> * At touch-down, ignition OFF
> * Fuel selector OFF
> * Fuel pump OFF
> * *(PoH 3.2.2)*

g) Engine failure in cruise

> * 70KIAS
> * Trim
> * Select appropriate landing area, free of obstacles, into wind if possible
> * Check/tighten safety harness
> * If possible, attempt engine restart
>   * Fuel pump ON
>   * Fuel selector ON
>   * Throttle 1/2 position
>   * Master ON
>   * Alternator ON
>   * Ignition ON
>   * Engage starter
> * Flaps as required
> * Check/tighten safety harness again
> * Communicate location and intentions (tx 7700)
> * Brief passenger
> * Immediately before touch-down, fuel selector OFF
> * Ignition OFF
> * Master switch OFF
> * Alternator OFF
> * Fuel pump OFF
> * *(PoH 3.4.1)*
> * *(PoH 3.2.4)*

h) Carbon Monoxide detected in the cabin in flight

* Heating CLOSE
* Open fresh ait intake
* Land ASAP

----

**4. Normal Procedures**

State, describe or detail:

a) the start sequence for

  * cold start

    > * Pre-flight       CMPL
    > * MR               CMPL
    > * Start POS          CK
    > * Taxi brief       CMPL
    > * PAX brief        CMPL
    > * Harness        SECURE
    > * Park brake         ON
    > * Fuel      emptiest/FR
    > * Switches          OFF
    > * Master             ON
    > * EFIS backup        ON
    > * Strobe light       ON
    > * Circuit Breaks     CK
    > * CHRG light         ON
    > * Fuel pump          ON
    > * Ignition 1&2       ON
    > * Choke              ON
    > * Throttle          SET
    > * “CLEAR PROP”
    > * Starter        ENGAGE       
    > * Oil Pressure       CK
    > * Choke    SLOW RELEASE
    > * RPM          SET 2000
    > * Fuel pump         OFF
    > * Fuel pressure      CK
    > * Avionics/EFIS/G5   ON
    > * CHRG light        OFF
    > * NAV light          ON
    > * Strobe light      OFF
    > * Flaps              UP

  * hot start

    > *same as cold start, except no choke*

b) The RPM used for checking the ignition system

> * 4000RPM
> * *(PoH 4.2.3)*

c) The maximum RPM when checking the ignition system

> * 3700RPM
> * *(PoH 4.2.3)*

d) The climb power setting, IAS and fuel flow

> * Climb power setting: 5500RPM
> * IAS: 72KIAS
> * Fuel Flow: 20L/hr *(PoH 5.4)*

e) A typical 65% power setting, IAS and fuel flow at 5000ft pressure height

> * The engine RPM at 65% power setting is 4800RPM *(Rotax 912 Series Operators Manual 5.1)*
> * The KIAS at 4800RPM at 3000ft is 87KIAS *(PoH 5.3)*
> * The KIAS at 4800RPM at 6000ft is 80KIAS *(PoH 5.3)*
> * The KIAS at 4800RPM at interpolation of (3000, 6000)ft to 5000ft is 85KIAS
> * The fuel consumption at 4800RPM at 3000ft is 15L/hr at 87KIAS *(PoH 5.4)*
> * The fuel consumption at 4800RPM at 5000ft does not exceed 15L/hr at 85KIAS

f) Using the aeroplane flight manual or POH, calculate the endurance for the aeroplane at 5000ft AMSL (ISA) with 65% power set.

> * Assume 15L/hr at 85KIAS *(answer e))*
> * Usable fuel: 146L
> * Endurance(hours) = 146/15 = 9.733 hours = 9 hours 44 minutes = 584 minutes

----

**5. Weight and Balance, and Performance**

Define and indicate the value of:

a) Maximum take-off weight

> * 600kg
> * The maximum weight of the aircraft, including fuel, persons on board, baggage, before flight
> * *(PoH 2.7)*

b) Maximum landing weight

> * 600kg
> * The maximum weight of the aircraft, including fuel, persons on board, baggage, before landing
> * *(PoH 2.7)*

c) Maximum number of adult persons on board (POB)

> 2

d) Maximum baggage weight

> * 35kg
> * The maximum weight of the baggage, in total, in both baggage compartments
> * Front section maximum: 35kg
> * Back section maximum: 25kg
> * *(PoH 7.6)*

e) Maximum fuel which can be carried with a full load of adult passengers (80kg/person) and maximum baggage weight

> * Standard Empty Weight: 370kg
> * Standard Empty Weight & PAX: 530kg
> * Standard Empty Weight & PAX & Baggage: 565kg
> * Maximum Fuel weight at MTOW: 35kg
> * Fuel weight per volume: 0.72kg/L
> * Maximum Fuel volume at MTOW: 48.6L
> * *(PoH 6)*

f) Using the aeroplane flight manual, determine the take-off weight and balance solution (Maximum take-off weight and C of G position), the max amount of fuel that can be carried and the endurance; for 2x 80kg POB and 20kg cargo.

> |           | Weight (kg) | Arm (mm) | Mom (mm/kg) |
> | --------- | ----------- | -------- | ----------- |
> | Pilot     | 80          | 1959     | 156720      |
> | PAX       | 80          | 1959     | 156720      |
> | Front bag | 20          | 2508     |  50160      |
> | Rear bag  | 0           | 2896     |      0      |
> | Fuel      |             | 1511     |             |
> | BEW       | 370         | 1653     | 611610      |

> * ZFW(kg): ∑{80,80,20,370} = 550
> * let Fuel = 50kg
> * Fuel(L) = 50 * 0.72 = 36.0
> * TOW = 600kg
> * Fuel Moment (mm/kg) = 50 * 1511 = 75550
> * Total Moment at ZFW (mm/kg) = ∑{156720,156720,50160,611610} = 975210
> * Total Moment at TOW (mm/kg) = 975210 + 75550 = 1050760
> * CG at ZFW = 975210 / 550 = 1773.11
> * CG at TOW = 1050760 / 600 = 1751.27
> * let Fuel burn rate = 18L/hour *(QRH 11)*
> * Endurance(hour) = 36 / 18 = 2

g) Calculate the take-off distance required at maximum take-off weight, 2500ft AMSL and OAT 30C, and the minimum landing distance at maximum landing weight for the conditions at above question.

> * let ISA = 15C
> * ΔISA = 30 - 15 = 15
> * Assume: bitumen, nil slope, nil tail wind

> *(QRH 5)*

> | Condition                    | Factor | Actual |
> | ---------------------------- | ------ | ------ |
> | Increase of 1000ft PA        | *1.10  | 2.5    |
> | Increase of 10C ambient temp | *1.10  | 1.5    |
> | Dry grass                    | *1.20  | 0      |
> | Wet grass                    | *1.30  | 0      |
> | 2% upslope                   | *1.10  | 0      |
> | TAIL of 10% at Vr            | *1.20  | 0      |
> | Soft ground                  | *1.25  | 0      |
> | CAO 20.7.4                   | *1.15  | 1.15   |

> * let TKOF distance nil factor(metre) = 230 *(PoH 5.1)*
> * factor = ∏{2.5,1.5,1.15} = 4.3125
> * TKOF distance(metre) = 230 * 4.3125 = 991.875

----

**6. Fuel Systems, Fuel and Fluids**

a) The correct grade of fuel

> * 91 Anti-Knock Index Octane MOGAS
> * *Pathfinder Aviation uses 95 or 98 Octane MOGAS*
> * *(PoH 1.4)*

b) Approved alternate fuel

> 100LL AVGAS
> * *(PoH 1.4)*

c) Location of fuel tanks and drain points

> * Fuel tanks are located within the wing leading edges *(PoH 1.4)*
> * Fuel drain points are located at the lowest point of each fuel tank *(PoH 7.19)*
> * Underneath each wing, close to the wing root

d) The total and usable fuel in each tank

> * Per tank:
>   * total fuel: 75L
>   * total usable: 72L
> * *(PoH 1.4)*

e) The position of the fuel tank vents

> * The vent line starts at the top of the fuel tank and is routed around the fuel tank, down, and exits underneath the wing.
> * The fuel vent exits on the bottom centre of each wing.

f) Whether the engine has a carburettor or fuel injection system

> Carburetor

g) State the **minimum** fuel required for a 1 hour flight (not considering school fuel policy)

  * Flight fuel ______ litres

  * Fixed reverse ______ litres ______ minutes

  * Total minimum fuel ______ litres

> * Assume no INTER or TEMPO *(AIP ENR 1.1 (11.8.2.4))*
> * let Fuel burn rate = 18L/hour *(QRH 11)*
> * let Fixed Fuel Reserve = 30 minutes *(CAAP 234-1(2) 4.1)*
> * Flight fuel = 18L
> * Fixed Fuel Reserve = 18L * 0.5 = 9L
> * Total minimum fuel = 18L + 9L = 27L

h) State the average fuel flow ______ L/hr

> 18L/hour *(QRH 11)*

i) Location of the fuel boost/auxiliary pump and when it should be used

> * The electric fuel pump is located after the fuel selector valve and fuel filter and before the mechanical pump. *(PoH 7.19)*
> * The switch is located in the cabin, accessible to the flight crew, typically on the left side switch panel.

j) What conditions apply to tank selection for take-off, landing and cruise

> Select fullest tank
> *(PoH 4.2.2, PoH 4.4.1)*

k) When refuelling to less than full tanks, what restrictions apply and how is the quantity checked

> The first 30L of fuel is not visible or detectable by dipstick, due to dihedral wing design.

l) The correct grade of oil for the aeroplane

> * RON424
> * *(Rotax 912 Series Operators Manual 2.5)*
> * *(Rotax Service Instruction SI-912-016)*

m) The minimum oil quantity before flight

> 2.05L  *(Rotax 912 Series Operators Manual 3.3)*

n) The maximum quantity of oil

> 2.5L *(PoH 1.4)*

o) The maximum, minimum and normal engine oil pressures

> * Maximum: 102psi cold engine starting
> * Minimum: 12psi <3500RPM
> * Normal: 29-73psi > 3500RPM
> * *(PoH 2.13)*

----

**7. Engine and Propeller Details**

a) What is the make/model of the engine?

> Rotax 912ULS

b) Number of cylinders and arrangement?

> 4 cylinders, horizontally opposed, naturally-aspirated, on a single crankshaft

c) Describe the lubrication system

> * Dry-sump system with a camshaft-driven mechanical pump and integrated pressure regulator.
> * The mechanical pump draws oil from the oil tank, via the oil radiator, forcing oil through the filter to the required points of lubrication.
> * Surplus oil accumulates on the bottom of the crankcase and forced back to the tank by piston blow-by.
> * The oil circuit is vented on the oil tank.
> * The oil temperature and pressure sensor are mounted with the mechanical oil pump.
> * *(Operators Manual Rotax Engine Type 912 Series (7.3))*

d) Describe the cooling system

> * The cooling system is liquid cooled cylinder heads, with ram-air cooling of cylinders.
> * The liquid cooling system is a closed circuit, with expansion tank and excess pressure valve.
> * Coolant flow is driven by a mechanical water pump driven by the camshaft.
> * Coolant flows from the pump, through the cylinder heads, to expansion tank, then through the radiator.
> * The closed circuit coolant system has a capacity of 3.0L.
> * *(Operators Manual Rotax Engine Type 912 Series (7.1))*

e) What is the maximum power output?

> * 98.6hp
> * Maximum: 5 minutes

f) What is the take-off power setting and time limit?

> * 98.6hp @ 5800RPM
> * Maximum: 5 minutes
> * *(PoH 2.13)*

g) What is the Maximum Continuous power and rpm?

* > 92.5hp @ 5500RPM
* > *(PoH 2.13)*

h) Idle speed?

> * 1400RPM
> * *(Operators Manual Rotax Engine Type 912 Series (2.2))*

i) The maximum, minimum and normal engine oil temperatures?

> * Maximum: 140C
> * Minimum: 50C
> * Normal: 90-100C
> * *(PoH 2.13)*

j) The maximum EGT temperature?

> * 880C
> * *(Operators Manual Rotax Engine Type 912 Series (2.2))*

k) The maximum engine coolant temperature?

> * 120C
> * *(Operators Manual Rotax Engine Type 912 Series (2.2))*

l) Propeller make and model?

> * The propeller is a Warp Drive 70 inch composite ground adjustable three blade propeller.
> * *(PoH 7.18)*

----

**8. Airframe**

a) What type is the undercarriage system (fixed/retractable/tricycle/conventional)

> * fixed tricycle
> * *(PoH 7.3)*

b) Which control surfaces can be trimmed?

> * elevator
> * *(PoH 7.2)*

c) What type of flap is installed in the Sling 2?

> * semi-slotted fowler type
> * *(PoH 7.1)*

d) Describe the flap actuating system

> * Transmotec electric flap actuator DLA-12-20-A-100-POT-IP65
> * Electronically controlled located on the instrument panel or control stick
> * *(PoH 6.1)*
> * *(PoH 7.2)*

e) Describe the flap indicating system

> The flap position is indicated on the MGL EFIS.

f) What flap settings are available?

> There are four flap settings, including no flap extension.

> **The Sling 2 PoH Rev 1.2 claims the flap limits are 0-32 degrees (+/-3), while the Sling 2 PoH Rev 2.3 claims the flap limits are 0-30 degrees (+/-3).**

g) Sketch the location of exits

> The exit for the aircraft is the sliding canopy.

h) Describe/sketch the location of the landing, taxi lights; fresh air intakes and fuel caps

> * The landing and taxi lights are on the left wing, close to the wing tip.
> * The fuel caps are located on the top centre of each wing, closer to the leading edge.
> * Engine air intake is on the right side of the fuselage, immediately behind the propellor.
> * Cabin air intake is on each side of the fuselage, in front of the cabin.

i) What is the wing span of the aeroplane?

> * 9.165 metres
> * *(PoH 1.4)*

----

**9. Ancillary Systems**

a) What systems are hydraulically operated?

> brakes

b) What procedures are followed when a hydraulic system failure is suspected?

> * The only hydraulic system is the brake system.
> * Land as slowly as possible.
> * Shut down engine on landing.
> * Avoid obstacles until aircraft comes to complete stop.

c) What are the sources of electrical power?

> * Main battery
> * Generator
> * EFIS backup

d) What is the DC system voltage?

> 12V DC

e) Can an external power source be used? If so, what is the procedure?

> No.

f) Where are the battery and external receptacle located?

> * The battery is on the engine side of the firewall
> * *(PoH 7.17)*

g) How long can the battery supply emergency power?

> * The engine ignition system is supplied by a dual CDI *(Rotax 912 Series Operators Manual 7.4)*.
> * The ignition system is independent of the battery and the battery cannot be used to supply backup power for the ignition system.
> * The battery will provide power, dependent on the power draw from the (non-ignition) sources of electrical power demand.

h) Following an alternator/generator failure in flight, which non-essential electric services should be switched off?

> All electrics, except ignition.

i) If a stall warning device is fitted, is it electrical or mechanical?

> * The stall warning device is electrical.
> * The pitot system is fitted with a tube that incorporates an Angle of Attack sensor. *(PoH 7.8)*
> * The AoA sensor is calibrated during flight testing to achieve a static pressure differential.

j) How is the cockpit ventilated?

> * Air enters the cabin through intakes on each side of the forward fuselage.
> * A fresh air vent is located on each side of the cockpit, each with a shut-off valve that can be rotated through 180 degrees.

k) How is the cockpit heated?

> * The cabin heat is controlled from the cockpit by a control knob located to the right of centre of the dashboard.
> * The control knob causes the fresh air intakes to go through a heat exchange with the exhaust muffler.
> * The heating system exhaust vent is located near the rudder pedals on the right side of the cabin.

l) Show the location of the following safety equipment:

  * fire extinguisher

    > mounted transverse, immediately behind and centre to the seats, in the baggage area

  * ELT

    > Glove box or centre console.

  * Torches

    > Glove box or centre console.

  * Survival equipment

    > Glove box or centre console.

  * First aid kit

    > Glove box or centre console.

----

**10. Flight instruments**

a) Where are the pitot head(s), static vent(s) and any water drain points for the pitot/static system located?

> * The pitot-static tube is located below the left wing *(PoH 7.8)*.
> * There is no static vent.
> * There is a water drain point on the rear of the pitot tube.

b) Is there a pitot head system fitted?

> * *head* is a typo -- should be *heat*.
> * There is no pitot heat system.

c) Is there an alternate static source fitted?

> No

d) List all pressure instruments installed in the Sling 2.

> * Airspeed Indicator
> * Altimeter
> * Vertical Speed indicator

e) If the pitot tube is blocked, what instrument/s will be affected?

> Airspeed Indicator

f) If the static port is blocked, what instrument/s will be affected?

> * Airspeed Indicator
> * Altimeter
> * Vertical Speed indicator

g) Which flight instruments are operated electrically?

> Turn coordinator

h) Which flight instruments are gyroscopically operated?

> * Turn coordinator (electric)
> * Attitude indicator
> * Heading indicator

i) Which instruments are operated by vacuum?

> * Attitude indicator
> * Heading indicator
