cao_20_7_4 = (*1.15)
kg_to_lb = (*2.2)
ft_to_metre = (*0.3048)
litre_to_kg_avgas = (*0.72)
taxi_fuel_kg = 5

-- Calculate pressure altitude

isapressure = 1013.25
pressure_altitude ele qnh = max 0 (fromIntegral ele + (isapressure - fromIntegral qnh) * 30)

-- Find the 4 numbers to interpolate
--     nw     ne
--     sw     se
-- Calculate the proportion of the temperature to interpolate (0 >= temp_prop <= 1)
-- Calculate the proportion of the pressure altutide to interpolate (0 >= pa_prop <= 1)

data DistanceRequired =
  DistanceRequired {
    distance_required_ft
      :: Double
  , distance_required_ft_cao20_7_4
      :: Double
  , distance_required_metre
      :: Double
  , distance_required_metre_cao20_7_4
      :: Double
 } deriving (Eq, Show)

f <<$>> DistanceRequired nw sw ne se =
  DistanceRequired (f nw) (f sw) (f ne) (f se)

eachDistanceRequired f (DistanceRequired nw1 sw1 ne1 se1) (DistanceRequired nw2 sw2 ne2 se2) =
  DistanceRequired (f nw1 nw2) (f sw1 sw2) (f ne1 ne2) (f se1 se2)

distanceRequired1 x =
  DistanceRequired x (cao_20_7_4 x) (ft_to_metre x) (cao_20_7_4 (ft_to_metre x))

grass gr clear50ft =
  let gr' = (*1.45) <<$>> gr
      t = eachDistanceRequired (+) (eachDistanceRequired (-) clear50ft gr) gr'
  in  (gr', t)

interpolate nw sw ne se temp_prop pa_prop =
  let interpolate' a b prop = (a-b)*prop+b
      interpolate_temp a b = interpolate' a b temp_prop
      result = interpolate' (interpolate_temp se sw) (interpolate_temp ne nw) pa_prop
  in distanceRequired1 result

----

-- * Type: C172N
-- * Aircraft: VH-KJR

fuel = 147 -- kg
zfw = 850.3 -- kg
auw = fuel + zfw

----

-- YBAF temp: 29C

ybaf_declared_density = 2500 -- ft
ybaf_ele = 65 -- ft
ybaf_pa = ybaf_declared_density + ybaf_ele == 2565
ybaf_interpolate nw sw ne se = interpolate nw sw ne se 0.9 2.565

todr_ybaf_fuelburn = taxi_fuel_kg -- kg
todr_ybaf_auw = kg_to_lb (auw - todr_ybaf_fuelburn) == 2183.06

-- *assume 2200lb**

-- Ground Roll

todr_ybaf_groundroll = ybaf_interpolate 905 995 975 1070 ==
  DistanceRequired {
    distance_required_ft = 1210.3925
  , distance_required_ft_cao20_7_4 = 1391.9513749999999
  , distance_required_metre = 368.927634
  , distance_required_metre_cao20_7_4 = 424.2667791
  }

-- Clear 50ft

todr_ybaf_50ft = ybaf_interpolate 1560 1835 1785 1975 ==
  DistanceRequired {
    distance_required_ft = 2271.6525
  , distance_required_ft_cao20_7_4 = 2612.400375
  , distance_required_metre = 692.3996820000001
  , distance_required_metre_cao20_7_4 = 796.2596343
  }

todr_ybaf_check = [todr_ybaf_auw, todr_ybaf_groundroll, todr_ybaf_50ft]

----

-- YCFN temp: 29C

ycfn_declared_density = 2400 -- ft
ycfn_ele = 1450 -- ft
ycfn_pa = ycfn_declared_density + ycfn_ele == 3850
ycfn_interpolate nw sw ne se = interpolate nw sw ne se 0.9 3.85

-- LDR YCFN

ldr_ycfn_fuelburn = todr_ycfn_fuelburn + litre_to_kg_avgas 35 -- kg
ldr_ycfn_auw = kg_to_lb (auw - ldr_ycfn_fuelburn) == 2127.62

-- *assume 2400lb**

ldr_ycfn_groundroll_paved = ycfn_interpolate 615 635 635 660

ldr_ycfn_50ft_paved = ycfn_interpolate 1395 1430 1430 1470

ldr_ycfn = grass ldr_ycfn_groundroll_paved ldr_ycfn_50ft_paved

ldr_ycfn_groundroll = fst ldr_ycfn ==
  DistanceRequired {
    distance_required_ft = 1054.6212500000001
  , distance_required_ft_cao20_7_4 = 1212.8144375
  , distance_required_metre = 321.44855700000005
  , distance_required_metre_cao20_7_4 = 369.66584055000004
  }

ldr_ycfn_50ft = snd ldr_ycfn ==
  DistanceRequired {
    distance_required_ft = 1905.8712500000001
  , distance_required_ft_cao20_7_4 = 2191.7519374999997
  , distance_required_metre = 580.9095570000001
  , distance_required_metre_cao20_7_4 = 668.0459905499999
  }

ldr_ycfn_check = [ldr_ycfn_auw, ldr_ycfn_groundroll, ldr_ycfn_50ft]

-- TODR YCFN

todr_ycfn_fuelburn = taxi_fuel_kg -- kg
todr_ycfn_auw = kg_to_lb (auw - todr_ycfn_fuelburn) == 2183.06

-- *assume 2200lb**

todr_ycfn_groundroll_paved = ycfn_interpolate 905 995 975 1070

todr_ycfn_50ft_paved = ycfn_interpolate 1560 1835 1785 1975

todr_ycfn = grass todr_ycfn_groundroll_paved todr_ycfn_50ft_paved

todr_ycfn_groundroll = fst todr_ycfn ==
  DistanceRequired {
    distance_required_ft = 1931.14625
  , distance_required_ft_cao20_7_4 = 2220.8181874999996
  , distance_required_metre = 588.613377
  , distance_required_metre_cao20_7_4 = 676.90538355
  }
  
todr_ycfn_50ft = snd todr_ycfn ==
  DistanceRequired {
    distance_required_ft = 3126.04625
  , distance_required_ft_cao20_7_4 = 3594.9531874999993
  , distance_required_metre = 952.8188970000001
  , distance_required_metre_cao20_7_4 = 1095.74173155
  }

todr_ycfn_check = [todr_ycfn_auw, todr_ycfn_groundroll, todr_ycfn_50ft]

----

check = 
 concat
   [
     [ybaf_pa]
   , todr_ybaf_check
   , [ycfn_pa]
   , ldr_ycfn_check
   , todr_ycfn_check
   -- , [ldr_ybaf_auw]
   -- , ldr_ybaf_check
   ]

allcheck =
  all id check
