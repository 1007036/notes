-- C172 TODR/LDR
-- =============

cao_20_7_4 = (*1.15)
kg_to_lb = (*2.2)
ft_to_metre = (*0.3048)
litre_to_kg_avgas = (*0.72)
taxi_fuel_kg = 5

-- Calculate pressure altitude

isapressure = 1013.25
pressure_altitude ele qnh = max 0 (fromIntegral ele + (isapressure - fromIntegral qnh) * 30)

-- Find the 4 numbers to interpolate
--     nw     ne
--     sw     se
-- Calculate the proportion of the temperature to interpolate (0 >= temp_prop <= 1)
-- Calculate the proportion of the pressure altutide to interpolate (0 >= pa_prop <= 1)

data DistanceRequired =
  DistanceRequired {
    distance_required_ft
      :: Double
  , distance_required_ft_cao20_7_4
      :: Double
  , distance_required_metre
      :: Double
  , distance_required_metre_cao20_7_4
      :: Double
 } deriving (Eq, Show)

distanceRequired1 x =
  DistanceRequired x (cao_20_7_4 x) (ft_to_metre x) (cao_20_7_4 (ft_to_metre x))

interpolate nw sw ne se temp_prop pa_prop =
  let interpolate' a b prop = (a-b)*prop+b
      interpolate_temp a b = interpolate' a b temp_prop
      result = interpolate' (interpolate_temp se sw) (interpolate_temp ne nw) pa_prop
  in distanceRequired1 result

----

-- * Type: C172N
-- * Aircraft: VH-KJR

fuel = 147 -- kg
zfw = 850.3 -- kg
auw = fuel + zfw

----

-- 20180214 YBAF-[YBCG]-YLIS-[YSPE]-YTWB-YBAF
-- ==========================================

-- YBAF

-- temp: 27C

ybaf_ele = 65 -- ft
ybaf_qnh = 1008 -- hPa
ybaf_pa = pressure_altitude ybaf_ele ybaf_qnh == 222.5
ybaf_interpolate nw sw ne se = interpolate nw sw ne se 0.7 0.2225

-- DistanceRequired YBAF

todr_ybaf_fuelburn = taxi_fuel_kg -- kg
todr_ybaf_auw = kg_to_lb (auw - todr_ybaf_fuelburn) == 2183.06

-- *assume 2200lb**

-- Ground Roll

todr_ybaf_groundroll = ybaf_interpolate 750 825 805 885 ==
  DistanceRequired {
    distance_required_ft = 805.96625
  , distance_required_ft_cao20_7_4 = 926.8611874999999
  , distance_required_metre = 245.658513
  , distance_required_metre_cao20_7_4 = 282.50728995
  }

-- Clear 50ft

todr_ybaf_50ft = ybaf_interpolate 1375 1510 1470 1615 ==
  DistanceRequired {
    distance_required_ft = 1473.095
  , distance_required_ft_cao20_7_4 = 1694.0592499999998
  , distance_required_metre = 448.99935600000003
  , distance_required_metre_cao20_7_4 = 516.3492594
  }

todr_ybaf_check = [todr_ybaf_auw, todr_ybaf_groundroll, todr_ybaf_50ft]

----

-- YBCG

-- temp: 29C

ybcg_ele = 21 -- ft
ybcg_qnh = 1008 -- hPa
ybcg_pa = pressure_altitude ybcg_ele ybcg_qnh == 178.5
ybcg_interpolate nw sw ne se = interpolate nw sw ne se 0.9 0.1785

-- LDR YBCG

ldr_ybcg_fuelburn = todr_ybaf_fuelburn + litre_to_kg_avgas 13 -- kg
ldr_ybcg_auw = kg_to_lb (auw - ldr_ybcg_fuelburn) == 2162.468

-- *assume 2400lb**

-- Ground Roll

ldr_ybcg_groundroll = ybcg_interpolate 550 570 570 590 ==
  DistanceRequired {
    distance_required_ft = 571.57
  , distance_required_ft_cao20_7_4 = 657.3055
  , distance_required_metre = 174.21453600000004
  , distance_required_metre_cao20_7_4 =200.34671640000002
  }

-- Clear 50ft

ldr_ybcg_50ft = ybcg_interpolate 1295 1325 1325 1360 ==
  DistanceRequired {
    distance_required_ft = 1328.15825
  , distance_required_ft_cao20_7_4 = 1527.3819875
  , distance_required_metre = 404.8226346
  , distance_required_metre_cao20_7_4 = 465.54602979
  }

ldr_ybcg_check = [ldr_ybcg_auw, ldr_ybcg_groundroll, ldr_ybcg_50ft]

----

-- DistanceRequired YBCG

todr_ybcg_fuelburn = ldr_ybcg_fuelburn + taxi_fuel_kg -- kg
todr_ybcg_auw = kg_to_lb (auw - todr_ybcg_fuelburn) == 2151.468

-- *assume 2200lb**

-- Ground Roll

todr_ybcg_groundroll = ybcg_interpolate 750 825 805 885 ==
  DistanceRequired {
    distance_required_ft = 813.69075
  , distance_required_ft_cao20_7_4 = 935.7443624999999
  , distance_required_metre = 248.0129406
  , distance_required_metre_cao20_7_4 = 285.21488168999997
  }

-- Clear 50ft

todr_ybcg_50ft = ybcg_interpolate 1375 1510 1470 1615 ==
  DistanceRequired {
    distance_required_ft = 1486.204
  , distance_required_ft_cao20_7_4 = 1709.1345999999999
  , distance_required_metre = 452.9949792
  , distance_required_metre_cao20_7_4 = 520.9442260799999
  }

todr_ybcg_check = [todr_ybcg_auw, todr_ybcg_groundroll, todr_ybcg_50ft]

----

-- YSPE

-- temp: 29C

yspe_ele = 2934 -- ft
yspe_qnh = 1008 -- hPa
yspe_pa = pressure_altitude yspe_ele ybcg_qnh == 3091.5
yspe_interpolate nw sw ne se = interpolate nw sw ne se 0.9 3.0915

-- LDR YSPE

ldr_yspe_fuelburn = todr_ybcg_fuelburn + litre_to_kg_avgas 38 -- kg
ldr_yspe_auw = kg_to_lb (auw - ldr_yspe_fuelburn) == 2091.276

-- *assume 2400lb**

-- Ground Roll

ldr_yspe_groundroll = yspe_interpolate 550 570 570 590 ==
  DistanceRequired {
    distance_required_ft = 629.83
  , distance_required_ft_cao20_7_4 = 724.3045
  , distance_required_metre = 191.97218400000003
  , distance_required_metre_cao20_7_4 = 220.76801160000002
  }

-- Clear 50ft

ldr_yspe_50ft = yspe_interpolate 1295 1325 1325 1360 ==
  DistanceRequired {
    distance_required_ft = 1428.65675
  , distance_required_ft_cao20_7_4 = 1642.9552625
  , distance_required_metre = 435.45457740000006
  , distance_required_metre_cao20_7_4 = 500.77276401000006
  }

ldr_yspe_check = [ldr_yspe_auw, ldr_yspe_groundroll, ldr_yspe_50ft]

----

-- DistanceRequired YSPE

todr_yspe_fuelburn = ldr_yspe_fuelburn + taxi_fuel_kg -- kg
todr_yspe_auw = kg_to_lb (auw - todr_yspe_fuelburn) == 2080.276

-- *assume 2200lb**

-- Ground Roll

todr_yspe_groundroll = yspe_interpolate 750 825 805 885 ==
  DistanceRequired {
    distance_required_ft = 1045.27425
  , distance_required_ft_cao20_7_4 = 1202.0653874999998
  , distance_required_metre = 318.5995914
  , distance_required_metre_cao20_7_4 = 366.38953011
  }

-- Clear 50ft

todr_yspe_50ft = yspe_interpolate 1295 1325 1325 1360 ==
  DistanceRequired {
    distance_required_ft = 1428.65675
  , distance_required_ft_cao20_7_4 = 1642.9552625
  , distance_required_metre = 435.45457740000006
  , distance_required_metre_cao20_7_4 = 500.77276401000006
  }

todr_yspe_check = [todr_yspe_auw, todr_yspe_groundroll, todr_yspe_50ft]

----

-- LDR YBAF

ldr_ybaf_fuelburn = todr_yspe_fuelburn + taxi_fuel_kg -- kg
ldr_ybaf_auw = kg_to_lb (auw - ldr_ybaf_fuelburn) == 2069.276

-- *assume 2400lb**

-- Ground Roll

ldr_ybaf_groundroll = ybaf_interpolate 550 570 570 590 ==
  DistanceRequired {
    distance_required_ft = 568.45
  , distance_required_ft_cao20_7_4 = 653.7175
  , distance_required_metre = 173.26356
  , distance_required_metre_cao20_7_4 = 199.253094
  }

-- Clear 50ft

ldr_ybaf_50ft = ybaf_interpolate 1295 1325 1325 1360 ==
  DistanceRequired {
    distance_required_ft = 1323.45375
  , distance_required_ft_cao20_7_4 = 1521.9718124999997
  , distance_required_metre = 403.38870299999996
  , distance_required_metre_cao20_7_4 = 463.89700844999993
  }

ldr_ybaf_check = [ldr_ybaf_auw, ldr_ybaf_groundroll, ldr_ybaf_50ft]

----

check = 
 concat
   [
     [ybaf_pa]
   , todr_ybaf_check
   , [ybcg_pa]
   , ldr_ybcg_check
   , todr_ybcg_check
   , [yspe_pa]
   , ldr_yspe_check
   , todr_yspe_check
   , [ldr_ybaf_auw]
   , ldr_ybaf_check
   ]

allcheck =
  all id check