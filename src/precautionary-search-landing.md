# Pre-cautionary search and landing

### Notes reversed by watching flight video

* Low flying configuration
  * 10 deg flap
  * land into wind

* First pass
  * 500ft AGL
  * 80KIAS
  * search for obstacles
  * time from start of runway to end of runway (seconds * 40)
  * PAX briefing

* Second pass
  * 200ft AGL
  * 80KIAS
  * 20 deg flap
  * abeam end of runway to 500ft AGL

* PAN PAN PAN PAN PAN PAN
* pre-landing checks
* short-field landing
