# Study topics of self-identified deficiencies

### Bob Tait PPL

* Pressure Altitude
  * formula
  * examples
* Density Altitude
  * formula
  * by flight computer
  * Declared DA charts
    * CAO 20.7.0
    * SUMMER
    * WINTER
    * AUTUMN-SPRING
* Loading
  * Different load types
  * V speeds
* TAS
  * by flight computer
  * formula (?)
  * examples
* Aeroplane categories
  * Normal, Utility, Aerobatic
  * Load Factor limits
* *up to page 17*

### Part 61 MoS

### Dyson & Holland exams

### Curtis Aviation exams

### RPL

### CASA

https://www.casa.gov.au/standard-page/ppl-sample-examination-questions

### Ongoing notes

* cloud OKTA
* synoptic charts
* remember time zones
* MET
  * Col
  * Trough
  * Front
  * Ridge
  * Rime Ice, Hoar Frost, Advection fog
  * Clouds
  * Wind
    * Katabatic
    * Anabatic
    * Mountain wave
  * light signals
  * VFR alternate
  * VFR minima
  * ADIZ
  * reportable matter
