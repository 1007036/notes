# Formation scratch notes

### Tony Morris 20201019

* Formation Aircraft names: Lead and Wing
* Echelon
  * 45 degrees either side
  * Lead's main wheels on horizon
    1. UP -- smooth pitch to place Lead's wheels on horizon
    2. FORWARD -- power to move to 45 deg position
    3. IN -- small roll to converge, then roll away to parallel
* Line Astern
  * Directly behind Lead
  * Lead's main wheels on horizon
  * Tailplane
    * same width as wing
    * leading edge over leading edge
* Line abreast
  * Directly abeam lead
  * Lead's main wheels on horizon, one behind other
* Key position
  * 1 aircraft length behind, 1 wingspan below echelon
  * used to become steady before final form position
* Rejoin
  1. Full power, 45deg AoB, small pitch up after 45deg heading change
  2. Reduce power to maintain <120kt, roll, pitch, dive to bring Lead above horizon
  3. Reposition on 45deg line
  4. Continue until "bloom" position, just before key
  5. Reduce power to idle to capture key position and set power to maintain
* Bug-out during rejoin
  1. Reduce power to idle
  2. Gentle pitch down
  3. No roll
  4. "bugging out left/right"
* Turning in Echelon
  1. Lead announces and turns first 30deg AoB
  2. Lead power
    * If Lead on inside of turn, increase IAS 5-10kt
    * If Lead on outside of turn, decrease IAS 5kt
  3. If Wing falling behind Lead, call "revs"
  4. If Wing catching up to Lead, call "power"
* Turning in Line Astern
  * Maintain lower main wheel on the horizon
  * Use standard line astern cues
* Radio/TX/Circuit
  * Taxi
    * Lead and Wing receive ATIS
    * Set frequency 123.45
    * Lead: "Ready"
    * Wing: "Ready" or "Standby"
    * Lead: "Flightscope push 119.9"
    * Wing: "Flightscope 2" and change to 119.9
    * Lead: "Flightscope 1"
    * Wing: "2"
    * Lead: "Archer Ground, Flightscope formation, 2x eurofox 4844 and 5350, POB 2+2, ETA, received ATIS"
    * Run-ups done together, Wing gives thumbs up to Lead when ready
    * Taxi to holding point in 45 degree formation, Lead on downwind side
  * Take-off
    * Lead: "Flightscope formationg holding..."
    * TWR: "cleared for take-off..."
    * Lead: cycle flight controls and release brakes -> Wing: release brakes
    * Lead: apply power slowly over 5 seconds to allow wing to keep up; maximum 4800-5000rpm
    * After take-off and climb as normal 
    * Lead: if Wing is lagging on outside of turn, pull lead and catch
    * Lead: if Wing is overtaking, apply power
    * Circuit turns: roll first to announce turn, then 30deg AoB
    * When clear of CTR, set 123.45MHz or next CTR
  * Join circuit
    * Lead: position outside of circuit -> avoid base on inside of Lead
    * Lead: base, final and land without flap
    * Wing: Land half or full flap
    * If crosswind
      * Lead on downwind side of centre line
      * Wind on upwind side
  * Roll out on runway
    * Lead: maintain their side of runway
    * Lead: avoid braking, roll through until Wing is seen under control
    * Wing: if landing will result in overtake
      * "Flightscope 1/2 hot lane, hot lane, hot lane"
  * Vacate runway, switch to ground
    * Lead: "Flightscope 1"
    * Wing: "2"
  * Lead: "push <frequency>" -> Lead will conduct a check on next frequency
  * Lead: "switch <frequency>" -> Change to frequency, no check will be done
  * Lost comms
    * hand signal -> point to ears
    * change to 123.45
  * Lead squawks, Wing in STBY or OFF
  * If formation split, both squawk

