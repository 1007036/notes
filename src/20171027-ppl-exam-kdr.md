# 20171027 PPLA Exam

#### Knowledge Deficiency Report, with self-assessment

* Result: **PASSED**

----

**Carriage of cargo _(Part 61 MOS Unit 1.5.3 PFRA, 2.4.1)_**

The question asked, relating to carriage of cargo on an unoccupied control seat:

* in all cases, flight controls must be removed *(I selected this one)*
* seat must be in full aft position
* must be hand luggage not more than 4kg
* MTOW must be < 5700kg

The answer is found is section 6 of CAO 20.16.2

> 6 CARGO IN PILOT COMPARTMENT

> 6.1 Carriage of cargo in pilot compartments is prohibited except that in aircraft having a maximum take-off weight not more than 5 700 kg, cargo may be carried on an unoccupied control seat.

> 6.2 Cargo carried on a control seat shall not exceed 77 kg in weight unless a seat loading scheme which would permit a greater weight is specifically approved by CASA.

> 6.3 Cargo shall not be carried on a control seat if the cargo or means of restraint would interfere with the operation of the aircraft.

> 6.3.1 When cargo is carried on a control seat, the flight controls relevant to that seat shall be removed where they have been designed for easy removal and the remaining fittings protected so as to prevent interference by the cargo to the operation of the aircraft.

----

**Factors affecting daylight hours _(Part 61 MOS Unit 1.7.1, PNVC 2.2.4)_**

I don't know why I got this question wrong.

----

**Calculate HDG and GS _(Part 61 MOS Unit 1.9.2 POPA, 2.5.1)_**

Given TAS 105, Wind 230/30, TR(T) 135 what is the most reasonable HDG and GS?

* 120 HDG, 98 knots GS
* 150 HDG, 105 knots GS
* 120 HDG, 98 knots GS
* 150 HDG, 105 knots GS

I know the answer is 150 HDG, 105 knots GS. However, I think I changed my answer when I was doing a final check. I don't know why.

----

**Determine Density Height _(Part 61 MOS Unit 1.9.2 POPA 2.3.1)_**
  
Given

  * OAT: +21C
  * QNH: 1009hPa
  * Aerodrome elevation: 380ft AMSL

I calculated Pressure Altitude incorrectly, correcting from ISA in the wrong direction. I have never knowingly made this oversight before.

----

**Take-off landing distance calculations _(Part 61 MOS Unit 1.9.2 POPA, 2.4.2)_**

I don't know why I got this question wrong.

----

**Determine weight and balance - BRAVO _(Part 61 MOS Unit 1.9.2 POPA, 2.4.2)_**

I am unclear why I got this wrong. There were two questions that used loading system BRAVO. One of them gave all weights in lbs, then asked how much fuel could be carried. The maximum fuel up to MTOW fell within the CG envelope, so I selected that. There was nothing related to taxi fuel on the loading system so I did not include it in the calculation.

The other question gave all parameters, except baggage. All those parameters were in lbs, except fuel required, which was 150L fuel, and I calculated to 237lb. I subtracted the total aircraft weight (minus baggage) from the MTOW and used this for baggage. However, I could not get the CG to arrive within the envelope, regardless of weight of baggage, and there was not an option to stop the flight.

I don't know which of the two questions relating to BRAVO that I got wrong.

----

**Interpret ARFOR _(Part 61 MOS Unit 1.8.2 PMTC, 2.7.3)_**

  There were two questions relating to ARFOR. One question gave a UTC time and asked for the lowest cloud base on a certain route. One of the questions gave a route that I was unable to determine, since the destination did not appear on the WAC. The location affected how to interpret the ARFOR.

**Interpret ARFOR _(Part 61 MOS Unit 1.8.2 PMTC, 2.7.3)_**

*This item appears twice in the KDR.*

----

**Lubrication systems _(Part 61 MOS Unit 1.1.1 BAKC, 3.4.2)_**

The oil pressure gauge is reading nil, then returning to normal. What is the most likely cause? I selected that the oil filter is blocked.

----

**Mixture control _(Part 61 MOS Unit 1.1.1 BAKC, 3.3.1)_**

I don't quite remember the details of this question. It was asking for mixture setting for best performance.

Two of the options were:

* just lean of maximum EGT (selected)
* between full rich and maximum EGT

I selected the first option of the two, and discarded all others. I considered the second one and considered changing my answer in the final check.

----

**Ammeter indications _(Part 61 MOS Unit 1.1.1 BAKC, 3.4.2(c)(iv))_**

A zero-center ammeter indicating positive indicates:

* battery is charging
* showing the current demand

There were other options. I selected the first one of the above. There was a diagram of the ammeter with the word "charge" in the positive direction. I fell for it.

----

**Drag & IAS _(Part 61 MOS Unit 1.1.2 RBKA, 3.1.1)_**

As airspeed decreases, induced drag

* decreases
* increases
* increases then decreases
* decreases then increases

Correct answer: **As airspeed decreases, induced drag increases. Additionally, parasitic drag decreases.**

----

**Turn performance _(Part 61 MOS Unit 1.1.2 RBKA, 3.5.3)_**

In a level turn, when IAS is increased:

* lift is increased
* rate of turn decreases
* radius of turn decreases

I selected the first one.

Correct answer: **rate of turn decreases (additionally, the radius of turn increases).**

----

**TEM basic principles _(Part 61 MOS Unit 1.6.1 PHFC, 2.15.1)_**

I am terrible at TEM and all the different classifications.
