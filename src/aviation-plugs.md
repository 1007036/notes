# General Aviation Audio plugs (aeroplane)

##### Speaker

* PJ-055

* M642/4-1

* 0.25 inch

* 6.35 mm

##### Microphone

* PJ-068

* M642/5-1

* 0.206 inch

* 5.23 mm
