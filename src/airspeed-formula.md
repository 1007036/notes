### Airspeed Formula

    D = Dynamic Pressure
    S = Static Pressure
    ρ = air density

    V = 1 / sqrt (2 (D - S) / ρ)
