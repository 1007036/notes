# 20180714 Eurofox 3K LSA self-assessment

### Circuit

* more back pressure on TO roll
* achieve 65 KIAS then 75 KIAS
* keep hand on throttle (trim once, then hand back)
* at 1000ft pull power (~90 mm travel) to 4000rpm
* maintain 1000ft, lift nose (trim)
* abeam threshold, power to 2500rpm
* maintain 1000ft, lift nose (trim)
* under 81KIAS (Vfe), take full flap
* turn base, achieve and maintain 65KIAS
* use power to maintain profile
* flare with nose to horizon (not higher)
* flap to half, trim to TO, 3 turns on throttle, then full

### Practice

* rudder and trim flying (no stick)
* sideslip: maintain centreline with aileron
