# List of things to remember to take for flying

* documents
  * **medical**
  * **licence**
  * **ASIC**
* flying tools
  * **garmin 62s**
  * **knee pad**
  * **headset**
  * **watch**
* **shoes/socks**
* **sunglasses**
* **drinking bottle**
* PAX brief
  * **smoking**
  * **seat belts**
  * **exits/doors**
  * **oxygen**
  * **floatation devices**
  * **luggage**
  * **onboard survival**
  * **_radio operation_**
* Live stream
  * **camera**
  * **audio cable**
  * **laptop**
