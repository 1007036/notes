# Constant Speed Propellor

### Part 1

https://www.youtube.com/watch?v=4ckZXoNTuhg

* Fixed pitch propellor
  * angle between plane of rotation and chord line is blade angle or pitch angle
  * higher blade angle at propellor root to produce even thrust along propellor blade
  * vertical component of relative air flow caused by propellor rotation
  * sum of vectors 1. vertical component 2. component of relative air flow by travelling forward = resultant relative air flow
  * AoA between propellor blade and resultant relative air flow decreases as forward speed increases, reducing total thrust
  * sum of vectors 1. propellor torque (opposite to propellor rotation direction) 2. thrust = resultant force
  * RPM increase causes increases vertical component of relative air flow and increased propellor AoA
  * Propellor efficiency is a consequence of
    * TAS
    * RPM
  * *todo draw diagram*
