# Flight operator forms

### Eurofox 24-7771

| Indicator    | Form       |
|:------------:|:----------:|
| Dynon Hobbs  | VDO        |
| Dynon Air    | *not used* |
| Analogue Air | TACH       |
