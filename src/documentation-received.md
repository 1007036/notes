### General Documents

* [CASR Part 61 Pilot Licensing](https://dl.dropboxusercontent.com/u/7810909/ppl/part61booklet.pdf)

  An overview of the structure, terminology and requirements of the new licensing scheme.

  Version number 2.0

  1301.1759

* [CASA Schedule 3 Part 61 Manual of standards](https://www.casa.gov.au/sites/g/files/net351/f/_assets/main/lib100212/part-61-instrument-vol3-schedule-3-new.pdf)

* [Archerfield ERSA](https://www.airservicesaustralia.com/aip/current/ersa/FAC_YBAF_12-Nov-2015.pdf)

  12 November 2015

* [Archerfield Visual Pilot Guide 2010](https://www.casa.gov.au/sites/g/files/net351/f/_assets/main/pilots/download/archer/archer.pdf)



----

### Received by email 15 December 2015

* Cessna

  Pilot's Operating Handbook And Flight Training Supplement

  SKYCATCHER

  Model 162

  This publication includes the material required to be furnished to the pilot by ASTM F2245.

  [pdf **272 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/Cessna-162-Skycatcher-POH.pdf)

* Departure Briefing

  [pdf **2 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/Departure%20Briefing.pdf)

* EXAMPLE RADIO CALLS YBAF

  [pdf **1 page**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/EXAMPLE%20RADIO%20CALLS%20YBAF.pdf)

* Garmin G300 Pilot's Guide

  [pdf **296 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/G300%20Pilot%20Guide.pdf)

* NEW STUDENT RADIO SHEET YBAF

  [pdf **2 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/NEW%20STUDENT%20RADIO%20SHEET%20YBAF.pdf)

* (CASA) Archerfield Visual Pilot Guide 2010

  [pdf **56 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151215/YBAF%20BOOK.PDF)

----

### Received by email 18 December 2015

* RADIO CALLS YBAF 

  [pdf **1 page**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151218/RADIO%20CALLS%20YBAF.PDF)

* Single Engine Piston Endorsement

  [doc **12 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151218/Single%20Engine%20Piston%20Endorsement.doc)

* EXAMPLE RADIO CALLS YBAF

  [pdf **1 page**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151218/EXAMPLE%20RADIO%20CALLS%20YBAF.PDF)

* Flight One Private Pilot requirements

  [pdf **2 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151218/Flight%20One%20Private%20Pilot%20requirements.pdf)

* NEW STUDENT RADIO SHEET YBAF

  [pdf **4 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151218/NEW%20STUDENT%20RADIO%20SHEET%20YBAF.PDF)

----

### Received in #pilots IRC 25 December 2015

* Flying the Garmin G1000

  *after discussions about True Air Speed*

  [pdf **24 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20151225/smx_g1000.pdf)

----

### Talk to Clint Dudman about exam study 30 December 2015

* Aviation Theory Centre Basic Aeronautical Knowledge (BAK)

  * Used for PPL exam study, along with [PPL & CPL (Aeroplane) Workbook](https://dl.dropboxusercontent.com/u/7810909/ppl/ppl-cpl-workbook.pdf).

  * Asks questions and references the PPL & CPL (Aeroplane) Workbook.

  * The CASA PPL & CPL (Aeroplane) Workbook is given at the exam.

* Aviation Theory Centre Flying Training Manual follows each flight sequence during PPL training.

----

### Received from David 08 January 2016

* Cessna 172S Pilot's Operating Handbook

  [pdf **369 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172SP_POH.pdf)

* VH-AFR Certificate of Registration

  [pdf **2 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/AFR%20Cert%20of%20Reg.PDF)

* Cessna 172S POH Specifications

  [pdf **16 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%2000%20Specifications.PDF)

* Cessna 172S POH General

  [pdf **30 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%2001%20General.PDF)

* Cessna 172S POH Limitations

  [pdf **16 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%2002%20Limitations.PDF)

* Cessna 172S POH Emergency Procedures

  [pdf **24 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%2003%20Emergency%20Procedures.PDF)

* Cessna 172S POH Normal Procedures

  [pdf **36 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%204%20Normal%20Procedures.PDF)

* Cessna 172S POH Performance

  [pdf **24 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%205%20Perfomance.PDF)

* Cessna 172S Weight and Balance

  [pdf **28 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%206%20Weight%20and%20Balance.PDF)

* Cessna 172S Systems Description

  [pdf **54 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%207%20Systems%20Description.PDF)

* Cessna 172S Handling and Service

  [pdf **24 pages**](https://dl.dropboxusercontent.com/u/7810909/ppl/20160106/C172S%20AFR/C172S%20POH%208%20Handling%20and%20service.PDF)

