# Navigation flight check list

* navigation log
* radio log, including VOR
* fuel log
* fuel plan
* top of descent planning
* ersa summary for each destination
* ersa entry (complete) for each destination
* danger and restricted areas log
* mark VTC
  * navigation route
  * 5nm marks
* mark WAC
  * navigation route
  * radio boundaries
  * airspace boundaries
  * 10nm marks
* flight notification form
* daylight
* TODR/LDR
* weight/balance
* check weather and NOTAM
* drink bottle
* food
* landing fees
* camera

#### after receiving wind forecast

* complete navigation log
  * HDG
  * G/S
  * ETI
  * EET
* fuel plan
* TODR/LDR
* finish flight notification form for flight levels

#### immediately before flight

* NOTAM
* GAF, GPWT, Location briefs
* submit flight notification form
* enter alert for cancel SARTIME
