# PPL exam material check list

* CAR 1988
  * *borrow from flight school*
* CASR Part 61
  * *borrow from flight school*
* CAO 20-95.2
  * *borrow from flight school*
  * *print CAR 20.7.0, CAR 20.7.4*
* CAAP 92-1(1) and 234-1(1)
  * *print*
* AIP Book
  * *borrow from flight school*
* ERSA
* AUS PCA
* Visual Flight Rules Guide (VFRG)
* Sydney WAC
* Townsville WAC
* Bourke WAC
* Scale ruler
* Protractor
* Flight Computer
* Basic calculator
