* GFPT/RPL (recreational licence)

* PPL ~ 60 hours (marketing says 45 hours)


* three segments
  1. pre-solo (0-15 hours)
  2. solo 
  3. area-solo (30 hours)

* 35hr recreational (RPL)
  * solo exam
  * area solo exam
  * radio operators exam

* 25hr navigation (PPL)
  * CASA issue PP theory exam

* early morning preferred due to air turbulence

* total $26000.00 including airfield fees.
