# Flight Exam Preparation

# 20180220

### FLIGHT TEST REPORT Private Pilot Licence - Aeroplane **[(Form 61-1488)](https://www.casa.gov.au/files/form61-1488pdf)**

##### GROUND COMPONENT

1. **Underpinning Knowledge required for items 9 to 59**

2. **Privileges and limitations of the private pilot licence with aeroplane category rating;**

  * can carry passengers if at least 3 take-offs and landings within previous 90 days **[CASR61.395](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.395.html)**
  * pilot holds and carries **[CASR61.420(b)](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.420.html)** class 1 or 2 medical certificate or RAMCP under conditions in subparagraph 2 **[CASR61.405](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.405.html)**
  * pilot must carry documents; licence, medical certificate, maintenance release, pilot operating handbook
  * must have English proficiency assessment **[CASR61.422](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.422.html)**
  * pilot only registered aircraft **[CASR61.425](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.425.html)**
  * single-engine aircraft, MTOW <= 1500kg, day VFR, private operation or flight training **[CASR61.460](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.460.html)**
  * CASR 61.E.1
    * **[CASR 61.375 Limitations on exercise of privileges of pilot licences--ratings](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.375.html)**
    * **[CASR 61.380 Limitations on exercise of privileges of pilot licences--flight activity and design feature endorsements](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.380.html)**
    * **[CASR 61.385 Limitations on exercise of privileges of pilot licences--general competency requirement](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.385.html)**
    * **[CASR 61.390 Limitations on exercise of privileges of pilot licences--operating requirements and limitations](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.390.html)**
    * **[CASR 61.395 Limitations on exercise of privileges of pilot licences--recent experience for certain passenger flight activities](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.395.html)**
    * **[CASR 61.400 Limitations on exercise of privileges of pilot licences--flight review](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.400.html)**
    * **[CASR 61.405 Limitations on exercise of privileges of pilot licences--medical requirements--recreational pilot licence holders](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.405.html)**
    * **[CASR 61.410 Limitations on exercise of privileges of pilot licences--medical certificates: private pilot licence holders](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.410.html)**
    * **[CASR 61.415 Limitations on exercise of privileges of pilot licences--medical certificates: commercial, multi-crew and air transport pilot licence holders](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.415.html)**
    * **[CASR 61.420 Limitations on exercise of privileges of pilot licences--carriage of documents](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.420.html)**
    * **[CASR 61.422 Limitations on exercise of privileges of pilot licences--aviation English language proficiency](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.422.html)**
    * **[CASR 61.425 Limitations on exercise of privileges of pilot licences--unregistered aircraft](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.425.html)**
    * **[CASR 61.427 Removal of certain pilot licence conditions about airspace](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.427.html)**
  * CASR 61.H.1
    * **[CASR 61.505 Privileges of private pilot licences](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.505.html)**
    * **[CASR 61.510 Limitations on exercise of privileges of private pilot licences--multi-crew operations](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.510.html)**
    * **[CASR 61.515 Requirements for grant of private pilot licences](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.515.html)**

3. **Applicability of drug and alcohol regulations;**

  * 8 hours from consumption of alcohol to departure
  * not intoxicated (0.02 grams per 210 litres of breath)
  * crew will not consume while on board
  * [CASA Medication](https://www.casa.gov.au/standard-page/medication)
  * "Always consult your DAME or CASA about the safe use of medication. Many may be used with appropriate guidance and safeguards."

4. **VFR aircraft instrument requirements;**

  * **CAO20.18(10)** Serviceability
  * **CAO20.18(Appendix 1)** Instruments required for flight under Visual Flight Rules  
    * airspeed indicator
    * altimeter
    * direct or remote magnetic compass
    * timepiece with hours, minutes, seconds
  * PoH Section 2 LIMITATIONS - KINDS OF OPERATIONS EQUIPMENT LIST
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=9)

5. **Emergency equipment requirements;**

  * life jackets and life rafts **CAO20.11(5)**
    * life jackets when over water and out of glide **CAO20.11(5.1)**
    * sufficient life raft(s) when min(30 minutes cruise)(100nm) **CAO20.11(5.2.1)**
  * emergency signalling equipment **CAO20.11(6)**
    * if life raft required, 1 ELT and pyro distress signals **CAO20.11(6.1)**
    * if more than one life raft required, then >= 2 (approved ELT under reg 252A) transmitters 121.5MHz and 243MHz and stowed ready for use **CAO20.11(6.1)**
    * single-engine, over water, not equipped with radio or incapable of air-to-ground radio, not required to carry a life raft, shall carry ELT (121.5MHz and 243MHz approved under reg 252A) **CAO20.11(6.2)**
  * briefing of passengers **CAO20.11(14)**
    * smoking requirements **CAO20.11(14.1.1)**
    * use of seat belts **CAO20.11(14.1.1)**
    * location of emergency exits **CAO20.11(14.1.1)**
    * use of oxygen where applicable **CAO20.11(14.1.1)**
    * use of floatation devices where applicable **CAO20.11(14.1.1)**
    * stowage of luggage **CAO20.11(14.1.1)**
    * onboard survival equipment **CAO20.11(14.1.1)**

6. **Fuel planning and oil requirements for the flight;**

  * PiC must take steps to ensure sufficient fuel and oil **[CAR1988(234) Fuel requirements](http://www.austlii.edu.au/au/legis/cth/consol_reg/car1988263/s234.html)**
  * Fixed fuel reserve for VFR, aeroplane, piston-engine: 45 minutes **[CAAP 234-1(1) GUIDELINES FOR AIRCRAFT FUEL REQUIREMENTS](https://www.casa.gov.au/file/104851/download?token=AC2dkxNS)**
  * 5-8 quarts of oil **Cessna 172 PoH Section 8 CAPACITY OF ENGINE SUMP**

7. **Managing cargo and passengers;**

  * **CAO20.16.2 AIR SERVICE OPERATIONS — LOADING — GENERAL**
    * Cargo on or above floor shall be restrained **CAO20.16.2(3)**
    * Cargo shall not obstruct flight controls **CAO20.16.2(4.1)**, emergency exits **CAO20.16.2(4.2)**
    * Cargo on a passenger seat shall evenly distribute weight, not exceeding 77kg **CAO20.16.2(5.1)** and restrained **CAO20.16.2(5.2)**
    * Cargo on unoccupied control seat shall not exceed 77kg **CAO20.16.2(6.2)**, restrained **CAO20.16.2(6.4)**, flight controls removed if easy **CAO20.16.2(6.3.1)**, not interfere with aircraft operation **CAO20.16.2(6.3)**
  * **CAO20.16.3** Air service operations — carriage of persons
    * seat belts during take-off, landing, < 1000ft AGL, turbulence **CAO20.16.3(4.1)**
    * one pilot crew wearing seat belt at all times **CAO20.16.3(4.2)**
    * seats upright during take-off and landing **CAO20.16.3(5.1)**
    * passenger in control seat must be given instruction, no interfere with flight controls **CAO20.16.3(11.1)**
    * two infants (<=3 years of age) may be carried on one seat with total weight <= 77kg **CAO20.16.3(13.1)**

8. **Cessna 172N PoH**

  * PoH Section 2 LIMITATIONS
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=11)
      * Vne = 160KIAS
      * Vno = 128KIAS
      * Va(2300) = 97KIAS
      * Va(1950) = 89KIAS
      * Va(1600) = 80KIAS
      * Vfe(10) = 110KIAS
      * Vfe(>10) = 85KIAS
      * Maximum window open = 160KIAS
  * PoH Section 3 EMERGENCY PROCEDURES
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=15)
      * EFATO(no flap) = 65KIAS
      * EFATO(flap) = 60KIAS
      * Best Glide = 65KIAS
      * Prec-search = 60KIAS
      * Land no power(no flap) = 65KIAS
      * Land no power (flap) = 60KIAS
      * S/F TKOF = 59KIAS
      * S/F LDG = 55-65KIAS
  * PoH Section 4 NORMAL PROCEDURES
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=24)
      * Vx = 59KIAS
      * Vy = 73KIAS
      * TKOF/LDG demonstrated crosswind = 15KIAS

9. **Communicating face-to-face**

10. **Operational communication using an aeronautical radio**

11. **Complete all required pre-flight actions and procedures**

  * [Maintenance guide for pilots](https://www.casa.gov.au/file/123536/download?token=ZahWNdEA)

----

### FLIGHT TEST REPORT Class Rating - Single Engine Aeroplane [(Form 61-1495)](https://www.casa.gov.au/file/159336/download?token=FzRyOA3S)

##### GROUND COMPONENT

1. **Underpinning Knowledge of 10 to 54**

2. **Privileges and limitations of the single engine aeroplane class rating**

  * pilot licence must demonstrate competency of part 61 MoS **[CASR61.400](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.400.html)**
  * flight review every 24 months, within 3 months of expiry, and valid to the end of that month **[CASR61.745](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.745.html)**

3. **Flight Review requirements;**

  * pilot licence must demonstrate competency of part 61 MoS **[CASR61.400](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.400.html)**
  * flight review every 24 months, within 3 months of expiry, and valid to the end of that month **[CASR61.745](http://www.austlii.edu.au/au/legis/cth/consol_reg/casr1998333/s61.745.html)**

4. **Navigating and operating systems;**

  *??*

5. **Normal, abnormal and emergency flight procedures;**

  * PoH Section 3 EMERGENCY PROCEDURES
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=15)

6. **Operating limitations**

  * PoH Section 2 LIMITATIONS
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=11)

7. **Weight and balance limitations**

  * PoH Section 6 WEIGHT AND BALANCE/EQUIPMENT LIST
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=47)

8. **Aircraft performance data, including take-off and landing performance data**

  * Add 15% to all take-off and landing distances for MTOW <= 2000kg **CAO20.7.4(6.1)**
  * PoH Section 5 PERFORMANCE
    * [Cessna 172N](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=36)
  * Short-Field Take-Off Distance
  * Short-Field Landing Distance
  * Obtain Terminal Area Forecast from [NAIPS](https://www.airservicesaustralia.com/naips/Briefing/Location)
    * wind speed and direction
    * QNH
    * temperature
 
9. **Flight planning**

  * Minimum fuel requirements
    * PiC must take steps to ensure sufficient fuel and oil **[CAR1988(234) Fuel requirements](http://www.austlii.edu.au/au/legis/cth/consol_reg/car1988263/s234.html)**
    * Fixed fuel reserve for VFR, aeroplane, piston-engine: 45 minutes **[CAAP 234-1(1) GUIDELINES FOR AIRCRAFT FUEL REQUIREMENTS](https://www.casa.gov.au/file/104851/download?token=AC2dkxNS)**

10. **Pre-flight actions and procedures**
  * Required documents to be carried
    * Maintenance Release
    * Pilot Operating Handbook
    * Aviation Medical Certificate
  * [Maintenance guide for pilots](https://www.casa.gov.au/file/123536/download?token=ZahWNdEA)

----

### Documentation

* [Cessna 172N manual](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf)
  * [TABLE OF CONTENTS](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=3)
  * [1 GENERAL](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=4)
  * [2 LIMITATIONS](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=9)
  * [3 EMERGENCY PROCEDURES](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=15)
  * [4 NORMAL PROCEDURES](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=24)
  * [5 PERFORMANCE](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=36)
  * [6 WEIGHT AND BALANCE/EQUIPMENT LIST](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=47)
  * [7 AIRPLANE AND SYSTEMS DESCRIPTION](http://data.tmorris.net/aviation/poh/c172n/Cessna_172_C172N-1978-POH-S1to7-scanned.pdf#page=54)
  * 8 AIRPLANE HANDLING, SERVICE AND MAINTENANCE *missing*
  * 9 SUPPLEMENTS *missing*
  